Rocket CMS Bundle
=================

This bundle can handle content with revision support, an ckEditor powered admin panel which can be attatched to your admin panel, some twig functions to display blocks, page content links and files path and a controller for displaying page content with a custom layout!

Summary
-------
* [Installation](#markdown-header-installation)
* [Configuration](#markdown-header-configuration)
* [Twig Functions](#markdown-header-twig-functions)

Installation
------------
You must add the custom repository to the composer file, as this bundle is not included in packagist.org.

    :::json
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:rocketbrasil/rocket-cms-bundle.git"
        }
    ]

Then, add to require:

    :::json
    "rocketbrasil/rocket-cms-bundle": "*"

Add the bundle to the `AppKernel.php` file:
    
    :::php
    new Rocket\CmsBundle\CmsBundle(),


Configuration
-------------
### config.yaml

Depending on what feature you enable, you must ad some configuration in your `config.yml`. None of the configuration keys are required, but depending on what feature you wish to use, this must be enabled too.

Here follows the configuration reference:

    :::yaml
    cms:
        # Static files base url
        base_url:             ~ # Example: http://static.example.com/cms

        # Static files path
        files_path:           ~ # Example: /some/path/for/cms/files

        # Frontend template is used to display page content types
        frontend_template:    ~ # Example: AcmeDemoBundle::layout.html.twig

        # Backend template is used to cms administration area
        backend_template:     ~ # Example: AcmeBackendDemoBundle::layout.html.twig

        # Theme for cms content properties
        content_form_theme:   ~ # Example: AcmeDemoBundle::form.html.twig

        # Theme for cms content serialized data
        serialized_data_form_theme:  ~ # Example: AcmeDemoBundle::anotherForm.html.twig

        # Enable the AWS S3 Storage for CMS files
        aws_s3_use:           false # Example: 1

        AWS S3 Storage Access Key
        aws_key:              ~ # Example: AHJKJTRYZ7KB7XNZIQ4Q

        # AWS S3 Storage Access Secret
        aws_s3_secret:           ~ # Example: 23hJGxEWZpAFAV1Hhc+A5xe1++aBoBC98BeCTab1

        # AWS S3 Storage Bucket name
        aws_s3_bucketname:       ~ # Example: bucket-name

        # Save files to default assets server
        save_in_server:       true # Example: 1

---

### routing.yml

To display the page content, you must provide the route to match the uri from cms contents.

##### Example:

    :::yaml
    frontend_cms:
        path:   "/{uri}"
        defaults: { _controller: CmsBundle:Cms:index }

In this example, the router will match everything after the domain. It **MUST** be the last route in your rounting configuration file, otherwise, it will match any route, ignoring the other routes.

For the admin panel, you must include the rest of controllers for the content management area.

##### Example:

    :::yaml
    backend_cms:
        resource: "@CmsBundle/Controller/"
        type:     annotation
        prefix:   /

You can also provide more configuration options, enforcing some domain or prefix as any other routing configuration.

---

### parameters.yml

To enable the CMS image storage in Amazon S3 you need to set the variable aws_use_s3 to true and the right 
credentials to access the S3 Bucket.

##### Example:

    :::yaml
    aws_bucketname: <bucket-name>
    aws_key: '<aws-access-key>'
    aws_secret: '<aws-access-secret>'
    aws_use_s3: true

Twig Functions
--------------
#### cms_path($cmsContentName, array $customAttr = array())

This function will return an anchor html tag for a page content, where title will be the title field used at the `PageContent`.

The `$cmsContentName` param is the page name.

The `$customAttr` param is an key-value array. It will include the html properties to the anchor tag.

###### Example:
    :::jinja
    {{ cms_path('acme-faq', { class: 'btn btn-primary' })|raw }}

---

#### cms_file_path($filename)

This function should be used in Content views. It concatenate the `cms.base_url` to the file name.

The `$filename` param is the file name.

###### Example:
    :::jinja
    {{ cms_file_path('file.jpg') }}

---
#### cms_block($cmsContentName)

This function will return an anchor html tag for a page content, where title will be the title field used at the `PageContent`.

The `$cmsContentName` param is the block name wich will be rendered.

###### Example:
    :::jinja
    {{ cms_block('acme-footer')|raw }}

