$(document).ready(function () {
    $('#cms-content-new-button').click(function () {
        var url = $.trim($('#cms-content-type-select').val());

        if (url !== '') {
            window.location = url;
        }
    });
});
