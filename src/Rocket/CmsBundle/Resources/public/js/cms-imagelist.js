$(document).ready(function() {
    var imageList = $('#rocket_cmsbundle_imagelisttype_image_files');
    var imageCount = $('input', imageList).length;

    $('#cmsbundle-imagelist-add').click(function() {
        var newWidget = imageList.data('prototype');
        newWidget = newWidget.replace(/__name__label__/g, imageCount);
        newWidget = newWidget.replace(/__name__/g, imageCount);
        imageList.append(newWidget);
        imageCount++;

        return false;
    });

    $('#cmsbundle-imagelist-remove').click(function() {
        var element = $('.control-group:last', imageList);

        if (element.length > 0) {
            element.remove();
            imageCount--;
        }
    });
})
