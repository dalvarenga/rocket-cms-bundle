<?php

namespace Rocket\CmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Rocket\CmsBundle\Entity\CmsContent;

class CmsContentRepository extends EntityRepository
{
    /**
     *
     * @param string $field
     * @param mixed  $value
     * @param string $locale
     *
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function findOneWithLocale($field, $value, $locale = null)
    {
        $query = $this->createQueryBuilder('cc')
            ->addSelect('ct')
            ->join('cc.cmsType', 'ct')
            ->where("cc.{$field} = :value")
            ->andWhere('cc.version = 0')
            ->setParameter(':value', $value)
            ->setMaxResults(1);

        if ($locale) {
            $query->andWhere('cc.locale = :locale')
                ->setParameter(':locale', $locale);
        }

        $result = $query
            ->getQuery()
            ->useResultCache(true)
            ->getOneOrNullResult();

        return $result;
    }

    /**
     * Get all entities of a CMS Type and locale.
     *
     * @param string $typeClass
     * @param string $locale
     * @return array
     */
    public function getAllByType($typeClass, $locale=null)
    {
        $query = $this->createQueryBuilder('cc')
            ->addSelect('ct')
            ->join('cc.cmsType', 'ct')
            ->where('cc.version = 0')
            ->andWhere("ct.class = :value")
            ->setParameter(':value', $typeClass);

        if ($locale) {
            $query->andWhere('cc.locale = :locale')
                ->setParameter(':locale', $locale);
        }

        $query = $query->getQuery();
        $query->useResultCache(true);

        return $query->getResult();
    }

    /**
     * @param \Rocket\CmsBundle\Entity\CmsContent $cmsContent
     */
    public function updateVersions(CmsContent $cmsContent)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $connection->executeUpdate("UPDATE cms_contents cc SET cc.version = (cc.version + 1) WHERE cc.locale = '{$cmsContent->getLocale()}' AND cc.name = '{$cmsContent->getName()}' ORDER BY cc.version DESC");
    }
}
