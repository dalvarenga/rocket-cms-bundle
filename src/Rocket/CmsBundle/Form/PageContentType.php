<?php

namespace Rocket\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

class PageContentType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'constraints' => array(
                    new Constraints\NotBlank()
                )
            ))
            ->add('metaDescription', 'text', array(
                'required' => false
            ))
            ->add('content', 'textarea', array(
                'attr' => array(
                    'class' => 'ckeditor'
                ),
                'constraints' => array(
                    new Constraints\NotBlank()
                )
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rocket_cmsbundle_pagecontenttype';
    }
}
