<?php

namespace Rocket\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rocket\CmsBundle\Entity\CmsType;

/**
 * @ORM\Table(name="cms_contents")
 * @ORM\Entity(repositoryClass="Rocket\CmsBundle\Repository\CmsContentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CmsContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Rocket\CmsBundle\Entity\CmsType
     *
     * @ORM\ManyToOne(targetEntity="CmsType", inversedBy="cmsContents")
     * @ORM\JoinColumn(name="cms_type_id", referencedColumnName="id")
     */
    protected $cmsType;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", nullable=false)
     */
    protected $locale;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", nullable=true)
     */
    protected $uri;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     */
    protected $version;

    /**
     * @var array
     *
     * @ORM\Column(name="serialized_data", type="array", nullable=false)
     */
    protected $serializedData = array();

    /**
     * @var bool
     *
     * @ORM\Column(name="require_auth", type="boolean")
     */
    protected $requireAuth;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get CMS Type
     *
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function getCmsType()
    {
        return $this->cmsType;
    }

    /**
     * Set CMS Type
     *
     * @param \Rocket\CmsBundle\Entity\CmsType $cmsType
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setCmsType(\Rocket\CmsBundle\Entity\CmsType $cmsType = null)
    {
        $this->cmsType = $cmsType;
        return $this;
    }

    /**
     * Get Locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set Locale
     *
     * @param string $locale
     * @return string
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get URI
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set URI
     *
     * @param string $uri
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * Get Version
     *
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set Version
     *
     * @param int $version
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get Serialized Data
     *
     * @return array
     */
    public function getSerializedData()
    {
        return $this->serializedData;
    }

    /**
     * Set Serialized Data
     *
     * @param array $serializedData
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setSerializedData($serializedData)
    {
        $this->serializedData = $serializedData;

        return $this;
    }

    /**
     * Get Require Auth
     *
     * @return boolean
     */
    public function getRequireAuth()
    {
        return $this->requireAuth;
    }

    /**
     * Set Require Auth
     *
     * @param boolean $requireAuth
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setRequireAuth($requireAuth)
    {
        $this->requireAuth = $requireAuth;

        return $this;
    }

    /**
     * Get Created At
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set Created At
     *
     * @param \DateTime $createdAt
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get Updated At
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set Updated At
     *
     * @param \DateTime $updatedAt
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtAsNow()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtAsNow()
    {
        $this->updatedAt = new \DateTime();
    }
}
