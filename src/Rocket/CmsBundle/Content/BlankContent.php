<?php

namespace Rocket\CmsBundle\Content;

/**
 * This is the fallback content when the cms block is not found.
 *
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 */
class BlankContent extends AbstractContent
{

    /**
     * @return \Symfony\Component\Form\AbstractType
     */
    public function getForm()
    {
        throw new CmsContentException("Blank content does not have a form.");
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return 'CmsBundle:Cms:blank.html.twig';
    }
}
