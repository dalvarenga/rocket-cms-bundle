<?php

namespace Rocket\CmsBundle\Content;

use Rocket\CmsBundle\Form\TextContentType;

/**
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 */
class TextContent extends AbstractContent
{

    /**
     * @return \Symfony\Component\Form\AbstractType
     */
    public function getForm()
    {
        $form = new TextContentType();

        return $form;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return 'CmsBundle:Cms:text.html.twig';
    }
}
