<?php

namespace Rocket\CmsBundle\Content;

use Rocket\CmsBundle\Form\PageContentType;

/**
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 */
class PageContent extends AbstractContent
{

    /**
     * @return \Symfony\Component\Form\AbstractType
     */
    public function getForm()
    {
        $form = new PageContentType();
        return $form;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return $this->container->getParameter('cms.page_content_template');
    }
}
